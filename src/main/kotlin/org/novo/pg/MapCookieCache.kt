package org.novo.pg

import okhttp3.Cookie
import okhttp3.CookieJar
import okhttp3.HttpUrl

class MapCookieCache : CookieJar {
    private val map = mutableMapOf<String, MutableList<Cookie>>()

    override fun saveFromResponse(url: HttpUrl, cookies: MutableList<Cookie>) {
        map[url.host()] = cookies
    }

    override fun loadForRequest(url: HttpUrl) = map[url.host()] ?: emptyList<Cookie>()

}
