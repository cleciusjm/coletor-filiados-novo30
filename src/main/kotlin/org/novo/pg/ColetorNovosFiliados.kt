package org.novo.pg

import okhttp3.OkHttpClient
import org.novo.pg.exceptions.LoginFailException

class ColetorNovosFiliados(val email: String, val senha: String) {

    val client by lazy {
        OkHttpClient.Builder()
                .cookieJar(MapCookieCache())
                .build()
    }

    fun coletar() =
            if (Login(email, senha)(client)) ActionSolicitacoesFiliacao()(client)
            else throw LoginFailException(email)

}