package org.novo.pg

data class Filiado(
        val id: String = "",
        val nome: String,
        val estado: String = "",
        val cidade: String = "",
        val titulo: String = ""
)