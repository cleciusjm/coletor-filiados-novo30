package org.novo.pg

import io.github.rybalkinsd.kohttp.dsl.httpGet
import okhttp3.Call
import org.jsoup.Jsoup

class ActionSolicitacoesFiliacao {

    operator fun invoke(client: Call.Factory): List<Filiado> {
        val body = httpGet(client) {
            scheme = "https"
            host = "novo.org.br"
            path = "/app/Solicitacoes"
        }.body()?.string()

        return Jsoup.parse(body).body().select("table tbody tr")
                .asSequence()
                .map { it.select("td") }
                .map {
                    Filiado(
                            nome = it[0].html(),
                            cidade = it[1].html(),
                            estado = it[2].html(),
                            titulo = it[3].html()
                    )
                }.toList()

    }
}