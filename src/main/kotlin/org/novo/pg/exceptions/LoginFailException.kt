package org.novo.pg.exceptions

class LoginFailException(val email: String) : Throwable()
