package org.novo.pg

import io.github.rybalkinsd.kohttp.dsl.httpPost
import okhttp3.Call

class Login(
        val username: String,
        val password: String
) {
    operator fun invoke(client: Call.Factory) = httpPost(client) {
        scheme = "https"
        host = "novo.org.br"
        path = "/app/login/verificalogin"
        header {
        }
        body("application/x-www-form-urlencoded") {
            form {
                "email" to username.trim()
                "senha" to password.trim()
            }
        }
    }.code().let { it < 400 }
}