package org.novo.pg

import okhttp3.OkHttpClient
import org.novo.pg.exceptions.LoginFailException

class BuscadorFiliados(val email: String, val senha: String, val busca: String) {

    val client by lazy {
        OkHttpClient.Builder()
                .cookieJar(MapCookieCache())
                .build()
    }

    fun buscar() =
            if (Login(email, senha)(client)) ActionBuscaComoConheceu(busca)(client)
            else throw LoginFailException(email)

}