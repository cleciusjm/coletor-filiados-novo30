package org.novo.pg

import io.github.rybalkinsd.kohttp.dsl.httpGet
import io.github.rybalkinsd.kohttp.dsl.httpPost
import io.github.rybalkinsd.kohttp.ext.asJson
import io.github.rybalkinsd.kohttp.util.Json
import okhttp3.Call
import org.jsoup.Jsoup

class ActionBuscaComoConheceu(val search: String) {

    operator fun invoke(client: Call.Factory): List<Filiado> {
        val response = httpPost(client) {
            scheme = "https"
            host = "novo.org.br"
            path = "/app/comoconheceu/search"
            body {
                form {
                    "filiado" to search
                }
            }
        }

        return response.asJson().map { Filiado(id = it["filiado_id"].asText(), nome = it["nome"].asText()) }

    }
}