package org.novo.pg

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.subcommands
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.prompt
import java.lang.Exception
import java.lang.IllegalArgumentException
import java.time.Instant.now
import java.time.temporal.ChronoUnit


fun main(args: Array<String>) {
    val start = now()
    Tool().subcommands(NovosFiliados(), BuscarFiliados()).main(args)
    println("Executado em ${start.until(now(), ChronoUnit.MILLIS)}ms")
}

class Tool : CliktCommand(help = "CLI para interação com site do novo") {
    override fun run() = Unit
}

class NovosFiliados : CliktCommand() {

    val email: String? by option(help = "Email para acesso a área do filiado").prompt("Seu email")
    val senha: String? by option(help = "Senha para acesso a área do filiado").prompt(text = "Sua senha", hideInput = true)
    val coletorFiliados by lazy {
        ColetorNovosFiliados(
                email = email ?: throw IllegalArgumentException("Email deve ser informado"),
                senha = senha ?: throw IllegalArgumentException("Senha deve ser informado")
        )
    }

    override fun run() {
        try {
            for (filiado in coletorFiliados.coletar()) println(filiado)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}

class BuscarFiliados : CliktCommand() {

    val email: String? by option(help = "Email para acesso a área do filiado").prompt("Seu email")
    val senha: String? by option(help = "Senha para acesso a área do filiado").prompt(text = "Sua senha", hideInput = true)
    val busca: String? by option(help = "Termo de busca").prompt("Pesquisa")
    val buscadorFiliados by lazy {
        BuscadorFiliados(
                email = email ?: throw IllegalArgumentException("Email deve ser informado"),
                senha = senha ?: throw IllegalArgumentException("Senha deve ser informado"),
                busca = busca ?: throw IllegalArgumentException("Termo de busca deve ser informado")
        )
    }

    override fun run() {
        try {
            for (filiado in buscadorFiliados.buscar()) println(filiado)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}