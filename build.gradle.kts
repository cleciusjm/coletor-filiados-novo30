import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val main = "org.novo.pg.MainKt"

group = "ogr.novo.pg"
version = "1.0-SNAPSHOT"
plugins {
    val kotlinVersion = "1.3.21"
    kotlin("jvm") version kotlinVersion
    kotlin("plugin.allopen") version kotlinVersion
    kotlin("plugin.noarg") version kotlinVersion
    id("com.palantir.graal") version "0.3.0-13-g722e029"
    application
}
repositories {
    jcenter()
    maven { url = uri("https://nexus.e-precise.com.br/repository/all") }
}
dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.jsoup:jsoup:1.11.3")
    implementation(group = "io.github.rybalkinsd", name = "kohttp", version = "0.9.0")
    implementation(group = "com.fasterxml.jackson.core", name = "jackson-core", version = "2.9.9")
    implementation(group = "com.fasterxml.jackson.core", name = "jackson-databind", version = "2.9.9")
    implementation("com.github.ajalt:clikt:1.7.0")
}
application {
    mainClassName = main
}
val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "1.8"
}
val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "1.8"
}
graal {
    mainClass(main)
    outputName("coletor-filiados")
    graalVersion("1.0.0-rc14")
    option("-H:+ReportUnsupportedElementsAtRuntime")
    option("--enable-https")
}
